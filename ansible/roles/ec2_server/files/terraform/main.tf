data "aws_ami" "amzn2" {
  count = var.ami_id == "" ? 1 : 0

  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn-ami-hvm-*-x86_64-gp2"]
  }
  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}

data "aws_secretsmanager_secret" "keypair" {
  name = var.keypair_secret
}

data "aws_secretsmanager_secret_version" "keypair" {
  secret_id = data.aws_secretsmanager_secret.keypair.id
}

locals {
  ami_id   = var.ami_id == "" ? data.aws_ami.amzn2.0.id : var.ami_id
  ssh_user = "ec2-user"
  keypair = jsondecode(data.aws_secretsmanager_secret_version.keypair.secret_string)
}

#tfsec:ignore:aws-ec2-no-public-ingress-sgr
#tfsec:ignore:aws-ec2-no-public-egress-sgr
resource "aws_security_group" "this" {
  name_prefix            = var.hostname
  description            = "${var.hostname} app server"
  revoke_rules_on_delete = true

  ingress {
    description = "HTTP to app port from anywhere"
    from_port   = var.http_port
    to_port     = var.http_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "SSH from anywhere"
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "ICMP from anywhere"
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    description = "Egress to anywhere"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = var.hostname
  }
}

resource "aws_instance" "this" {
  instance_type = var.instance_type
  ami           = local.ami_id
  key_name      = local.keypair["name"]

  vpc_security_group_ids = [aws_security_group.this.id]
  monitoring             = true

  root_block_device {
    encrypted = true
  }

  metadata_options {
    http_endpoint = "enabled"
    http_tokens = "required"
  }

  user_data = templatefile(
    "${path.module}/templates/userdata.tftpl", {
      hostname = var.hostname
      domain   = var.domain_name
      ssh_port = var.ssh_port
    }
  )

  tags = {
    Name = var.hostname
  }
}

resource "null_resource" "host_is_ready" {
  triggers = {
    instance_id = aws_instance.this.id
    instance_ip = aws_instance.this.public_ip
    instance_state = aws_instance.this.instance_state
  }

  provisioner "remote-exec" {
    connection {
      host        = aws_instance.this.public_ip
      port        = var.ssh_port
      user        = local.ssh_user
      private_key = local.keypair["private_key"]
    }

    inline = [
      "while [ ! -f /var/lib/cloud/instance/boot-finished ]; do echo 'Waiting for cloud-init...'; sleep 1; done"
    ]
  }
}
