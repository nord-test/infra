locals {
  tfstate_secret = "terraform-state"
}

# STATE NAME SECRET

resource "aws_secretsmanager_secret" "tfstate" {
  name = local.tfstate_secret

  kms_key_id = aws_kms_key.default.arn
  force_overwrite_replica_secret = true
  recovery_window_in_days        = 0

  tags = {
    Name = local.tfstate_secret
  }
}

resource "aws_secretsmanager_secret_version" "tfstate" {
  secret_id     = aws_secretsmanager_secret.tfstate.id
  secret_string = jsonencode({
    bucket   = aws_s3_bucket.tfstate.bucket
    table    = aws_dynamodb_table.tfstate.name
    region = aws_s3_bucket.tfstate.region
  })
}

# STATE LOCK TABLE

resource "aws_dynamodb_table" "tfstate" {
  name           = var.terraform_state_name
  read_capacity  = 1
  write_capacity = 1
  # billing_mode = "PAY_PER_REQUEST"

  hash_key = "LockID"

  attribute {
    name = "LockID"
    type = "S"
  }

  server_side_encryption {
    enabled = true
    kms_key_arn = aws_kms_key.default.arn
  }

  point_in_time_recovery {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }
}

# STATE S3 BUCKET

resource "aws_s3_bucket" "tfstate" {
  bucket = var.terraform_state_name

  lifecycle {
    prevent_destroy = true
  }
}

resource "aws_s3_bucket_versioning" "tfstate" {
  bucket = aws_s3_bucket.tfstate.id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_logging" "tfstate" {
  bucket = aws_s3_bucket.tfstate.id
  target_bucket = aws_s3_bucket.tfstate.id
  target_prefix = "log/"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "tfstate" {
  bucket = aws_s3_bucket.tfstate.id
  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.default.arn
      sse_algorithm     = "aws:kms"
    }
  }
}

resource "aws_s3_bucket_public_access_block" "tfstate" {
  bucket = aws_s3_bucket.tfstate.id
  block_public_policy     = true
  block_public_acls       = true
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket_ownership_controls" "tfstate" {
  bucket = aws_s3_bucket.tfstate.id
  rule {
    object_ownership = "BucketOwnerPreferred"
  }
}

resource "aws_s3_bucket_acl" "tfstate" {
  depends_on = [
    aws_s3_bucket_ownership_controls.tfstate
  ]
  bucket = aws_s3_bucket.tfstate.id
  acl    = "private"
}
