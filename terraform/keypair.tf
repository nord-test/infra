locals {
  keypair_name = "deploy"
  keypair_secret = "key-pair"
}

resource "tls_private_key" "keypair" {
  algorithm = "RSA"
  rsa_bits  = "2048"
}

resource "aws_key_pair" "keypair" {
  key_name   = local.keypair_name
  public_key = tls_private_key.keypair.public_key_openssh
}

resource "aws_secretsmanager_secret" "keypair" {
  name = local.keypair_secret

  kms_key_id = aws_kms_key.default.arn
  force_overwrite_replica_secret = true
  recovery_window_in_days        = 0

  tags = {
    Name = local.keypair_secret
  }
}

resource "aws_secretsmanager_secret_version" "keypair" {
  secret_id     = aws_secretsmanager_secret.keypair.id
  secret_string = jsonencode({
    name = aws_key_pair.keypair.key_name
    private_key = tls_private_key.keypair.private_key_openssh
    public_key = tls_private_key.keypair.public_key_openssh
  })
}
