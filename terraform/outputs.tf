output "keypair_name" {
  value = aws_key_pair.keypair.key_name
}

output "keypair_secret_name" {
  value = aws_secretsmanager_secret.keypair.name
}

output "tfstate_name" {
  value = var.terraform_state_name
}

output "tfstate_secret_name" {
  value = aws_secretsmanager_secret.tfstate.name
}
