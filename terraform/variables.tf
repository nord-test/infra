variable "region" {
  description = ""
  type        = string
  default     = "eu-north-1"
}

variable "terraform_state_name" {
  description = ""
  type        = string
  default     = "nord-test-terraform"
}
