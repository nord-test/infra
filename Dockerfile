# Stage 1: BASE
FROM python:3.9-slim-bullseye as base
WORKDIR /root
RUN apt-get update \
    && apt-get install --no-install-recommends -y curl libarchive-tools git gnupg software-properties-common openssh-client \
    && rm -rf /var/lib/apt/lists/*

# Stage 2: TERRAFORM
FROM base as terraform
RUN keyring=/usr/share/keyrings/hashicorp-archive-keyring.gpg \
    && ( curl -s https://apt.releases.hashicorp.com/gpg | gpg --dearmor > $keyring ) \
    && gpg --no-default-keyring --keyring $keyring --fingerprint \
    && echo "deb [signed-by=$keyring] https://apt.releases.hashicorp.com $(lsb_release -cs) main" > /etc/apt/sources.list.d/hashicorp.list \
    && apt-get update \
    && apt-get install --no-install-recommends -y terraform \
    && rm -rf /var/lib/apt/lists/*

# Main stage - install other tools + copy TERRAFORM
FROM base

# ANSIBLE, ANSIBLE-LINT, BOTOCORE, BOTO3
RUN pip install --no-cache-dir ansible ansible-lint botocore boto3

# AWS-CLI
RUN tmpdir="$(mktemp -d)" \
    && (curl -s "https://awscli.amazonaws.com/awscli-exe-linux-$(arch).zip"  | bsdtar -xvf- -C "$tmpdir") \
    && chmod -R +x "$tmpdir/aws" \
    && "$tmpdir/aws/install" \
    && rm -rf "$tmpdir"

# HADOLINT
RUN repo='https://github.com/hadolint/hadolint' \
    && arch="$(arch | sed -re 's/aarch64/arm64/')" \
    && version="$(curl -sI $repo/releases/latest | sed -re '/^location: /!d' -e 's|^location: .*/v([0-9\.]+)|\1|' -e 's/\r//')" \
    && curl -s -L "$repo/releases/download/v$version/hadolint-Linux-$arch" \
       | install --mod=755 /dev/stdin /usr/local/bin/hadolint

# AQUASEC TRIVY
RUN repo='https://github.com/aquasecurity/trivy' \
    && arch="$(arch | sed -re 's/(aarch64|arm64)/ARM/' -e 's/x86_64/64bit/')" \
    && version="$(curl -sI ${repo}/releases/latest | sed -re '/^location: /!d' -e 's|^location: .*/v([0-9\.]+)|\1|' -e 's/\r//')" \
    && curl -sL "${repo}/releases/download/v${version}/trivy_${version}_Linux-${arch}.tar.gz" \
       | tar -xzO trivy \
       | install --mod=755 /dev/stdin /usr/local/bin/trivy

# AQUASEC TFSEC
RUN curl -s 'https://raw.githubusercontent.com/aquasecurity/tfsec/master/scripts/install_linux.sh' | bash

# GITLAB-TERRAFORM
RUN curl -s 'https://gitlab.com/gitlab-org/terraform-images/-/raw/master/src/bin/gitlab-terraform.sh' \
    | install --mode=755 /dev/stdin /usr/local/bin/gitlab-terraform

# TERRAFORM
COPY --from=terraform /usr/bin/terraform /usr/local/bin/terraform

RUN adduser --disabled-password --gecos '' --home /work worker
USER worker
ENV ANSIBLE_HOST_KEY_CHECKING False
WORKDIR /work
ENTRYPOINT []
CMD [ "/bin/bash" ]
