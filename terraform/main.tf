provider "aws" {
  region = var.region
}

resource "aws_kms_key" "default" {
  enable_key_rotation = true
}
