output "hostname" {
  value = var.hostname
}

output "fqdn" {
  value = aws_instance.this.public_dns
}

output "http_port" {
  value = var.http_port
}

output "ssh_user" {
  value = local.ssh_user
}

output "ssh_port" {
  value = var.ssh_port
}

output "base_url" {
  value = "http://${aws_instance.this.public_dns}${ var.http_port == 80 ? "" : ":${var.ssh_port}" }"
}
