variable "instance_type" {
  description = ""
  type        = string
  default     = "t3.micro"
}

variable "ami_id" {
  description = ""
  type        = string
  default     = "ami-08fdff97845b0d82e"
}

variable "hostname" {
  description = ""
  type        = string
}

variable "domain_name" {
  description = ""
  type        = string
  default     = "local"
}

variable "http_port" {
  description = ""
  type        = number
  default     = 80
}

variable "ssh_port" {
  description = ""
  type        = number
  default     = 22
}

variable "keypair_secret" {
  description = ""
  type        = string
  default     = "key-pair"
}
